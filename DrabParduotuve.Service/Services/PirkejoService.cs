﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrabParduotuve.Domain.ViewModels;
using MySql.Data.MySqlClient;
using DrabParduotuve.Service;

namespace DrabParduotuve.Services
{
    public class PirkejoService : DefaultService
    {
        public PirkejoService()
        {

        }

        public Pirkejas SavePirkejas(Pirkejas pirkejas)
        {
            try
            {
                OpenConnection();
                MySqlCommand savePirkejas = new MySqlCommand();
                savePirkejas.Connection = DrabPardConnection;
                if (pirkejas.Id.HasValue)
                {
                    var cmdCommand = "update Pirkejas set Vardas = @Vardas, Pavarde = @Pavarde, GimimoData = @GimimoData WHERE ID = @ID;";
                    savePirkejas.CommandText = cmdCommand;
                    savePirkejas.Parameters.AddWithValue("@Vardas", pirkejas.Vardas);
                    savePirkejas.Parameters.AddWithValue("@Pavarde", pirkejas.Pavarde);
                    savePirkejas.Parameters.AddWithValue("@GimimoData", pirkejas.GimimoData);
                    savePirkejas.Parameters.AddWithValue("@ID", pirkejas.Id.Value);
                    savePirkejas.ExecuteNonQuery();
                }
                else
                {
                    var cmdCommand = "insert into Pirkejas (Vardas, Pavarde, GimimoData) values (@Vardas, @Pavarde, @GimimoData);";
                    savePirkejas.CommandText = cmdCommand;
                    savePirkejas.Parameters.AddWithValue("@Vardas", pirkejas.Vardas);
                    savePirkejas.Parameters.AddWithValue("@Pavarde", pirkejas.Pavarde);
                    savePirkejas.Parameters.AddWithValue("@GimimoData", pirkejas.GimimoData);
                    savePirkejas.ExecuteNonQuery();
                    pirkejas.Id = Convert.ToInt32(savePirkejas.LastInsertedId);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save Pirkejas.");
            }
            CloseConnection();
            return pirkejas;
        }

        public Uzsakymas saveUzsakyma (Uzsakymas uzsakymas)
        {
            OpenConnection();
            MySqlCommand saveUzsakyma = new MySqlCommand();
            saveUzsakyma.Connection = DrabPardConnection;

            var cmdCommand = "insert into Uzsakymas (PirkejoID, UzsakymoData) VALUES (LAST_INSERT_ID(), @UzsakymoData);";
            saveUzsakyma.CommandText = cmdCommand;
            saveUzsakyma.Parameters.AddWithValue("@UzsakymoData", uzsakymas.Data);
            saveUzsakyma.ExecuteNonQuery();
            uzsakymas.ID = Convert.ToInt32(saveUzsakyma.LastInsertedId);
            return uzsakymas;
        }

        public PrekesViewModel SavePreke(PrekesViewModel preke)
        {
            OpenConnection();
            MySqlCommand savePreke = new MySqlCommand();
            savePreke.Connection = DrabPardConnection;

            var cmdCommand = "insert into Prekiu_Uzsakymas(PrekesID, UzsakymoID) VALUES(@PrekesID,LAST_INSERT_ID());";
            savePreke.CommandText = cmdCommand;
            savePreke.Parameters.AddWithValue("@PrekesID", preke.PrekesID);
            savePreke.ExecuteNonQuery();
            preke.PrekesID = Convert.ToInt32(savePreke.LastInsertedId);
            return preke;
        }

        public List<Pirkejas> GetPirkejas(int? pirkejoID = null)
        {
            List<Pirkejas> pirkejai = new List<Pirkejas>();
            var cmdText = "SELECT * FROM Pirkejas WHERE (@ID IS NULL OR ID= @ID);";
            var cmdGetPirkejai = new MySqlCommand(cmdText, DrabPardConnection);
            cmdGetPirkejai.Parameters.AddWithValue("@Id", pirkejoID);

            OpenConnection();
            var pirkejasReader = cmdGetPirkejai.ExecuteReader();
            while (pirkejasReader.Read())
            {
                pirkejai.Add(new Pirkejas()
                {
                    Id = pirkejasReader.GetInt32("ID"),
                    Vardas = pirkejasReader.GetString("Vardas"),
                    Pavarde = pirkejasReader.GetString("Pavarde"),
                    GimimoData = pirkejasReader.GetString("GimimoData")
                });
            }
            CloseConnection();
            return pirkejai;
        }

        public List<Uzsakymas> GetOrders(Pirkejas pirkejoID)
        {
            List<Uzsakymas> uzsakymai = new List<Uzsakymas>();
            var cmdText = "SELECT * FROM Uzsakymas uz INNER JOIN Pirkejas p ON uz.PirkejoID = p.ID where PirkejoID = @PirkejoID";
            var cmdGetUzsakymai = new MySqlCommand(cmdText, DrabPardConnection);
            cmdGetUzsakymai.Parameters.AddWithValue("@PirkejoID", pirkejoID.Id);

            OpenConnection();
            var UzsakymoReader = cmdGetUzsakymai.ExecuteReader();
            while (UzsakymoReader.Read())
            {
                uzsakymai.Add(new Uzsakymas()
                {
                    ID = UzsakymoReader.GetInt32("ID"),
                    Vardas = UzsakymoReader.GetString("Vardas"),
                    Pavarde = UzsakymoReader.GetString("Pavarde"),
                    Data = UzsakymoReader.GetString("UzsakymoData")
                });
            }
            CloseConnection();
            return uzsakymai;
        }

        public void DeletePirkejas(int pirkejoID)
        {
            var deletePirkejasCmdText = "Delete from Prekiu_Uzsakymas where UzsakymoID =  (select ID from Uzsakymas where PirkejoID = @ID); delete from Uzsakymas where PirkejoID = @ID; delete from Pirkejas where ID = @ID;";
            var deleteBookCmd = new MySqlCommand(deletePirkejasCmdText, DrabPardConnection);
            deleteBookCmd.Parameters.AddWithValue("@ID", pirkejoID);
            OpenConnection();
            deleteBookCmd.ExecuteNonQuery();
            CloseConnection();
        }

        public List<PrekesViewModel> GetPirkejoPrekes (int uzsakymoID)
        {
            List<PrekesViewModel> drabuziai = new List<PrekesViewModel>();
            var cmdText = "select p.Pavadinimas, p.ID from Prekes p, Prekiu_Uzsakymas pu where p.ID = pu.PrekesID AND pu.UzsakymoID = @UzsakymoID;";
            var cmdGetParameters = new MySqlCommand(cmdText, DrabPardConnection);
            cmdGetParameters.Parameters.AddWithValue("@UzsakymoID", uzsakymoID);
            OpenConnection();
            var prekesReader = cmdGetParameters.ExecuteReader();
            while (prekesReader.Read())
            {
                drabuziai.Add(new PrekesViewModel()
                {
                    Pavadinimas = prekesReader.GetString("Pavadinimas"),
                    PrekesID = prekesReader.GetInt32("ID")
                });
            }
            CloseConnection();
            return drabuziai;
        }

        public List<PrekesViewModel> GetPrekes (int? prekesID = null)
        {
            List<PrekesViewModel> VisosPrekes = new List<PrekesViewModel>();
            var cmdText = "SELECT * FROM Prekes WHERE (@ID IS NULL OR ID= @ID);";
            var cmdGetPrekes = new MySqlCommand(cmdText, DrabPardConnection);
            cmdGetPrekes.Parameters.AddWithValue("@ID", prekesID);

            OpenConnection();
            var pirkejasReader = cmdGetPrekes.ExecuteReader();
            while (pirkejasReader.Read())
            {
                VisosPrekes.Add(new PrekesViewModel()
                {
                    Pavadinimas = pirkejasReader.GetString("Pavadinimas"),
                    PrekesID     = pirkejasReader.GetInt32("ID")
                });
            }
            CloseConnection();
            return VisosPrekes;
        }
    }
}
