﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Reflection;
using DrabParduotuve.Service;

namespace DrabParduotuve.Services
{
    public class DefaultService : DbConnection
    {
        public DefaultService()
        {

        }

        protected void OpenConnection()
        {
            if (DrabPardConnection.State == System.Data.ConnectionState.Closed)
            {
                DrabPardConnection.Open();
            }
        }

        protected void CloseConnection()
        {
            if (DrabPardConnection.State == System.Data.ConnectionState.Open)
            {
                DrabPardConnection.Close();
            }
        }		
	}
}
