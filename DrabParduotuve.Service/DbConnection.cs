﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace DrabParduotuve.Service
{
    public class DbConnection
    {
        protected MySqlConnection DrabPardConnection { get; private set; }

        public DbConnection()
        {
            DrabPardConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["DrabPardConnection"].ConnectionString);
        }
    }
}
