﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrabParduotuve.Domain.ViewModels
{
    public class Pirkejas
    {
        public int? Id { get; set; }

        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public string GimimoData { get; set; }


    }
}
