﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrabParduotuve.Domain.ViewModels
{
    public class Uzsakymas
    {
        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public string Data { get; set; }

        public int ID { get; set; }
    }
}
