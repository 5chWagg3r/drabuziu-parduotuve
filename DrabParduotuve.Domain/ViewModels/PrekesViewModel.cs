﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrabParduotuve.Domain.ViewModels
{
    public class PrekesViewModel
    {
        public int? PrekesID { get; set; }

        public string Pavadinimas { get; set; }
    }
}
