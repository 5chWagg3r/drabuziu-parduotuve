create schema `DrabuziuParduotuve`;
use `DrabuziuParduotuve`;
drop schema `DrabuziuParduotuve`;

create table Pirkejas
(
	Vardas varchar(100) not null,
    Pavarde varchar(100) not null,
    GimimoData varchar(50) not null,
    ID int primary key auto_increment
);

create table Prekes
(
	Pavadinimas varchar(100) not null,
	ID int primary key auto_increment
);

create table Uzsakymas
(
	PirkejoID int not null,
    UzsakymoData date not null,
    ID int primary key auto_increment,
    foreign key (PirkejoID) references Pirkejas(ID)
);

create table Prekiu_Uzsakymas
(
	PrekesID int,
	UzsakymoID int,
    ID int primary key auto_increment,
    foreign key (PrekesID) references Prekes(ID),
	foreign key (UzsakymoID ) references Uzsakymas(ID)
);
create table Parametrai
(
	ID int primary key auto_increment,
    PrekesID int,
    Pavadinimas varchar(50),
    verte varchar(50),
    foreign key (PrekesID) references Prekes(ID)
);

insert into Pirkejas (Vardas, Pavarde, GimimoData) 
VALUES
('Petras', 'Petrauskaitis', '1999-01-05'),
('Adomas', 'Abarius', '2005-03-17'),
('Edita', 'Andriukaitienė', '2001-03-20'),
('Evelina', 'Apeikytė', '1995-07-02'),
('Lukas', 'Atkočaitis', '2007-12-20'),
('Nojus', 'Bačiulis', '1999-09-30'),
('Danielius', 'Balčikonis','2003-06-01');

insert into Prekes (Pavadinimas)
VALUES
('Suknele'),
('Dzinsai'),
('Marskiniai'),
('Striuke'),
('Batai'),
('Ziedas');
insert into Uzsakymas (PirkejoID, UzsakymoData)
VALUES
(1, DATE("2017-02-08")),
(3, DATE("2017-12-15")),
(2, DATE("2018-01-05")),
(3, DATE("2018-01-08")),
(5, DATE("2018-03-08"));

insert into Parametrai(Pavadinimas, verte, PrekesID)
Values
('Medziaga','Medvilnė',1),
('Medziaga', 'Medvilnė', 2),
('Medziaga', 'Medvilnė', 3),
('Medziaga', 'Oda', 4),
('Medziaga', 'Oda', 5),
('Medziaga', 'Auksas', 6),
('Spalva', 'Raudona', 1),
('Spalva', 'Melyna', 2),
('Spalva', 'Balta', 3),
('Spalva', 'Ruda', 4),
('Spalva', 'Juoda', 5),
('Spalva', 'Geltona', 6),
('Kaina', '12.99$' , 1),
('Kaina', '15.99$' , 2),
('Kaina', '9.99$' , 3),
('Kaina', '25.99$' , 4),
('Kaina', '70.99$' , 5),
('Kaina', '50.15$' , 6);

insert into Prekiu_Uzsakymas(PrekesID, UzsakymoID) values
(1, 1),
(2, 2),
(3, 2),
(4, 3),
(5, 4);

select * from Uzsakymas where PirkejoID = 3;

select * from Pirkejas;

select * from Parametrai where PrekesID = 5;

select p.Pavadinimas, p.ID from Prekes p, Prekiu_Uzsakymas pu where p.ID = pu.Clothes_ID AND pu.Request_ID = 2;


select PrekesID from Prekiu_Uzsakymas where UzsakymoID = 2;


SELECT * FROM Uzsakymas uz INNER JOIN Pirkejas p ON uz.PirkejoID = p.ID where PirkejoID = 3;

