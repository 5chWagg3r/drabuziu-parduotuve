﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrabParduotuve.Domain.ViewModels;
using DrabParduotuve.Service;

namespace DrabParduotuve.Presentation
{
    public partial class Pirkejai : Form
    {
        private Services.PirkejoService _PirkejoService;
        private List<Pirkejas> _pirkejas;
        
        public Pirkejai()
        {
            InitializeComponent();
            _PirkejoService = new Services.PirkejoService();
            ConfigurePirkejoDataGrid();
            FillGrid();

        }
        private void FillGrid()
        {
            _pirkejas = _PirkejoService.GetPirkejas();
            PirkejuDataGridView.Rows.Clear();
            foreach (var pirkejas in _pirkejas)
            {
                int index = PirkejuDataGridView.Rows.Add();
                PirkejuDataGridView.Rows[index].Cells["PirkejoID"].Value = pirkejas.Id;
                PirkejuDataGridView.Rows[index].Cells["PirkejoVardas"].Value = pirkejas.Vardas;
                PirkejuDataGridView.Rows[index].Cells["PirkejoPavarde"].Value = pirkejas.Pavarde;
                PirkejuDataGridView.Rows[index].Cells["PirkejoGimimoData"].Value = pirkejas.GimimoData;
                PirkejuDataGridView.Rows[index].Cells["PirkėjoObjektas"].Value = pirkejas;
                PirkejuDataGridView.Rows[index].Cells["Uzsakymai"].Value = "Open";
                PirkejuDataGridView.Rows[index].Cells["Redaguoti"].Value = "Redaguoti";
                PirkejuDataGridView.Rows[index].Cells["Istrinti"].Value = "Istrinti";

            }
        }

        public void ConfigurePirkejoDataGrid()
        {
            PirkejuDataGridView.Columns.Add("PirkejoID", "Pirkejo ID");
            PirkejuDataGridView.Columns["PirkejoID"].Visible = false;
            PirkejuDataGridView.Columns["PirkejoID"].Name = "PirkejoID";
            PirkejuDataGridView.Columns["PirkejoID"].CellTemplate = new DataGridViewTextBoxCell();


            DataGridViewColumn PirkejoVardas = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Vardas",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "PirkejoVardas",
                DataPropertyName = "PirkejoVardas",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            PirkejuDataGridView.Columns.Add(PirkejoVardas);

            DataGridViewColumn PirkejoPavarde = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Pavarde",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "PirkejoPavarde",
                DataPropertyName = "PirkejoPavarde",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            PirkejuDataGridView.Columns.Add(PirkejoPavarde);

            DataGridViewColumn PirkejoGimimoData = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Gimimo Data",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "PirkejoGimimoData",
                DataPropertyName = "PirkejoGimimoData",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            PirkejuDataGridView.Columns.Add(PirkejoGimimoData);

            DataGridViewColumn PirkėjoObjektas = new DataGridViewColumn
            {
                Name = "PirkėjoObjektas",
                DataPropertyName = "PirkėjoObjektas",
                CellTemplate = new DataGridViewTextBoxCell(),
                Visible = false
            };
            PirkejuDataGridView.Columns.Add(PirkėjoObjektas);

            DataGridViewButtonColumn PrekiuDataButton = new DataGridViewButtonColumn
            {
                Name = "Uzsakymai",
                Width = 70,
                HeaderText = "Uzsakymai",
                Text = "Uzsakymai",
                UseColumnTextForButtonValue = true,
                CellTemplate = new DataGridViewButtonCell()
            };
            PirkejuDataGridView.Columns.Add(PrekiuDataButton);

            DataGridViewButtonColumn RedaguotiButton = new DataGridViewButtonColumn
            {
                Name = "Redaguoti",
                Width = 90,
                HeaderText = " ",
                Text = "Redaguoti",
                UseColumnTextForButtonValue = true,
                CellTemplate = new DataGridViewButtonCell()
            };
            PirkejuDataGridView.Columns.Add(RedaguotiButton);

            DataGridViewButtonColumn IstrintiButton = new DataGridViewButtonColumn
            {
                Name = "Istrinti",
                Width = 90,
                HeaderText = " ",
                Text = "Istrinti",
                UseColumnTextForButtonValue = true,
                CellTemplate = new DataGridViewButtonCell()
            };
            PirkejuDataGridView.Columns.Add(IstrintiButton);



            PirkejuDataGridView.CellContentClick += PirkejuDataGridView_CellContentClick;
        }

        private void PirkejuDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                //Uzsakymai uzsakymai = new Uzsakymai();
                //uzsakymai.Show();
                //var PirkejoID = ((Pirkejas)(((DataGridView)sender).Rows[e.RowIndex].Cells["PirkėjoObjektas"].Value)).Id;
                
                var uzsakymai = new Uzsakymai(_pirkejas[e.RowIndex]);
                uzsakymai.Show();
            }

            // UPDATE ******
            if (e.ColumnIndex == 6)
            {
                var pirkejas = (Pirkejas)((DataGridView)sender).Rows[e.RowIndex].Cells["PirkėjoObjektas"].Value;

                PakeistiPirkeja pakeistiPirkeja = new PakeistiPirkeja(pirkejas);
                pakeistiPirkeja.FormClosed += pakeistiPirkeja_FormClosed;
                pakeistiPirkeja.ShowDialog();
            }
            // DELETE ******
            if (e.ColumnIndex == 7)
            {
                DialogResult dialogResult = MessageBox.Show("Delete", "Ar tikrai norite istrinti sita pirkeja?", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    var pirkejoID = ((Pirkejas)((DataGridView)sender).Rows[e.RowIndex].Cells["PirkėjoObjektas"].Value).Id;
                    _PirkejoService.DeletePirkejas(pirkejoID.Value);
                }
            }
        }

        private void pakeistiPirkeja_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            Pirkejai pirkejas = new Pirkejai();
            pirkejas.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

