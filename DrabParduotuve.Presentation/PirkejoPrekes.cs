﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrabParduotuve.Domain.ViewModels;
using ViewModels = DrabParduotuve.Domain.ViewModels;

namespace DrabParduotuve.Presentation
{
    public partial class PirkejoPrekes : Form
    {
        private Services.PirkejoService _PirkejoService;
        private List<ViewModels.PrekesViewModel> _prekes;
        ViewModels.Uzsakymas _uzsakymoID;
        public PirkejoPrekes(ViewModels.Uzsakymas uzsakymoID)
        {
            InitializeComponent();
            _uzsakymoID = uzsakymoID;
            _PirkejoService = new Services.PirkejoService();
            ConfigurePirkejoDataGrid();
            FillGrid();

        }

        private void FillGrid()
        {
            _prekes = _PirkejoService.GetPirkejoPrekes(_uzsakymoID.ID);
            PirkejoPrekesDataGridView.Rows.Clear();
            foreach (var pirkejuPrek in _prekes)
            {
                int index = PirkejoPrekesDataGridView.Rows.Add();
                PirkejoPrekesDataGridView.Rows[index].Cells["PirkprekeID"].Value = pirkejuPrek.PrekesID;
                PirkejoPrekesDataGridView.Rows[index].Cells["PrekesPavadinimas"].Value = pirkejuPrek.Pavadinimas;

            }

        }

        public void ConfigurePirkejoDataGrid()
        {
            PirkejoPrekesDataGridView.Columns.Add("PirkprekeID", "PirkprekeID");
            PirkejoPrekesDataGridView.Columns["PirkprekeID"].Visible = false;
            PirkejoPrekesDataGridView.Columns["PirkprekeID"].Name = "PirkprekeID";
            PirkejoPrekesDataGridView.Columns["PirkprekeID"].CellTemplate = new DataGridViewTextBoxCell();


            DataGridViewColumn PrekesPavadinimas = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Pavadinimas",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "PrekesPavadinimas",
                DataPropertyName = "Pavadinimas",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            PirkejoPrekesDataGridView.Columns.Add(PrekesPavadinimas);

        }
    }
}
