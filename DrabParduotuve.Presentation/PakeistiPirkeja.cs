﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrabParduotuve.Domain.ViewModels;
using DrabParduotuve.Service;
using ViewModels = DrabParduotuve.Domain.ViewModels;

namespace DrabParduotuve.Presentation
{
    public partial class PakeistiPirkeja : Form
    {
        private Pirkejas _pirkejas;
        private Services.PirkejoService _pirkejoService;
        public List<Pirkejas> pirkejai { get; set; }
        ViewModels.Pirkejas _pirkejoID;
        public PakeistiPirkeja(ViewModels.Pirkejas pirkejoID)
        {
            _pirkejoID = pirkejoID;
            _pirkejoService = new Services.PirkejoService();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _pirkejas = _pirkejoService.SavePirkejas(_pirkejoID);
            _pirkejas.Vardas = txtVardas.Text;
            _pirkejas.Pavarde = txtPavarde.Text;
            _pirkejas.GimimoData = txtGData.Text;
            _pirkejoService.SavePirkejas(_pirkejas);

            txtVardas.Text = "";
            txtPavarde.Text = "";
            txtGData.Text = "";
        }
    }
}
