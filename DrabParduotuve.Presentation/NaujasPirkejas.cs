﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrabParduotuve.Services;
using DrabParduotuve.Domain.ViewModels;
namespace DrabParduotuve.Presentation
{
    public partial class NaujasPirkejas : Form
    {
        //protected List<int> list = new List<int>();
        private Pirkejas _pirkejas;
        private Uzsakymas _uzsakymai;
        private PrekesViewModel _preke;

        private Services.PirkejoService _pirkejoService;

        public NaujasPirkejas()
        {
            InitializeComponent();
            _preke = new PrekesViewModel();
            _pirkejoService = new Services.PirkejoService();
            PrepareForm();
        }
        private void PrepareForm()
        {
            listBoxPrekes.DataSource = new BindingSource(_pirkejoService.GetPrekes(), null);
            listBoxPrekes.DisplayMember = "Pavadinimas";
            //listBoxPrekes.ValueMember = "ID";
            listBoxPrekes.SelectedIndex = -1;

        }

        private void txtUzsakymoID_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var date = DateTime.Now.ToString();
            _pirkejas = new Pirkejas();
            _pirkejas.Vardas = txtVardas.Text;
            _pirkejas.Pavarde = txtPavarde.Text;
            _pirkejas.GimimoData = txtGData.Text;
            _pirkejoService.SavePirkejas(_pirkejas);

            _uzsakymai = new Uzsakymas();
            _uzsakymai.Data = DateTime.Now.ToString();
            _pirkejoService.saveUzsakyma(_uzsakymai);

            
            _preke.PrekesID = listBoxPrekes.SelectedIndex + 1;
            _pirkejoService.SavePreke(_preke);

            //foreach (var selectedItem in listBoxPrekes.SelectedItems)
            //{
            //    .BookAuthores.Add(new AuthorViewModel { Id = ((AuthorViewModel)selectedAutor).Id });
            //}
            //_authorService.SaveBook(_book);



            txtVardas.Text = "";
            txtPavarde.Text = "";
            txtGData.Text = "";

        }
    }
}
