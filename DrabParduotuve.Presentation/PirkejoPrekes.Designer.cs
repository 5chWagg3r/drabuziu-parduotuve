﻿namespace DrabParduotuve.Presentation
{
    partial class PirkejoPrekes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PirkejoPrekesDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.PirkejoPrekesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // PirkejoPrekesDataGridView
            // 
            this.PirkejoPrekesDataGridView.AllowUserToAddRows = false;
            this.PirkejoPrekesDataGridView.AllowUserToDeleteRows = false;
            this.PirkejoPrekesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PirkejoPrekesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PirkejoPrekesDataGridView.Location = new System.Drawing.Point(12, 12);
            this.PirkejoPrekesDataGridView.Name = "PirkejoPrekesDataGridView";
            this.PirkejoPrekesDataGridView.ReadOnly = true;
            this.PirkejoPrekesDataGridView.Size = new System.Drawing.Size(542, 222);
            this.PirkejoPrekesDataGridView.TabIndex = 0;
            // 
            // PirkejoPrekes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 259);
            this.Controls.Add(this.PirkejoPrekesDataGridView);
            this.Name = "PirkejoPrekes";
            this.Text = "PirkejoPrekes";
            ((System.ComponentModel.ISupportInitialize)(this.PirkejoPrekesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView PirkejoPrekesDataGridView;
    }
}