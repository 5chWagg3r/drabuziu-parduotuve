﻿namespace DrabParduotuve.Presentation
{
    partial class NaujasPirkejas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxPrekes = new System.Windows.Forms.ListBox();
            this.txtVardas = new System.Windows.Forms.TextBox();
            this.txtPavarde = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUzsakymoID = new System.Windows.Forms.TextBox();
            this.txtGData = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxPrekes
            // 
            this.listBoxPrekes.FormattingEnabled = true;
            this.listBoxPrekes.Location = new System.Drawing.Point(61, 124);
            this.listBoxPrekes.Name = "listBoxPrekes";
            this.listBoxPrekes.Size = new System.Drawing.Size(158, 147);
            this.listBoxPrekes.TabIndex = 0;
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(118, 16);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(122, 20);
            this.txtVardas.TabIndex = 1;
            // 
            // txtPavarde
            // 
            this.txtPavarde.Location = new System.Drawing.Point(118, 52);
            this.txtPavarde.Name = "txtPavarde";
            this.txtPavarde.Size = new System.Drawing.Size(122, 20);
            this.txtPavarde.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Vardas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Pavarde";
            // 
            // txtUzsakymoID
            // 
            this.txtUzsakymoID.Location = new System.Drawing.Point(284, 12);
            this.txtUzsakymoID.Name = "txtUzsakymoID";
            this.txtUzsakymoID.Size = new System.Drawing.Size(34, 20);
            this.txtUzsakymoID.TabIndex = 5;
            this.txtUzsakymoID.Visible = false;
            this.txtUzsakymoID.TextChanged += new System.EventHandler(this.txtUzsakymoID_TextChanged);
            // 
            // txtGData
            // 
            this.txtGData.Location = new System.Drawing.Point(118, 87);
            this.txtGData.Name = "txtGData";
            this.txtGData.Size = new System.Drawing.Size(122, 20);
            this.txtGData.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "GimimoData";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(61, 290);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 26);
            this.button1.TabIndex = 8;
            this.button1.Text = "Issaugoti";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NaujasPirkejas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 337);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGData);
            this.Controls.Add(this.txtUzsakymoID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPavarde);
            this.Controls.Add(this.txtVardas);
            this.Controls.Add(this.listBoxPrekes);
            this.Name = "NaujasPirkejas";
            this.Text = "NaujasPirkejas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxPrekes;
        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.TextBox txtPavarde;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUzsakymoID;
        private System.Windows.Forms.TextBox txtGData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}