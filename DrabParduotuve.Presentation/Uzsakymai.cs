﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrabParduotuve.Service;
using ViewModels = DrabParduotuve.Domain.ViewModels;

namespace DrabParduotuve.Presentation
{
    public partial class Uzsakymai : Form
    {
        private Services.PirkejoService _PirkejoService;
        private List<ViewModels.Uzsakymas> _uzsakymai;
        ViewModels.Pirkejas _pirkejoID;
        public Uzsakymai(ViewModels.Pirkejas pirkejoID)
        {
            InitializeComponent();
            _pirkejoID = pirkejoID;
            _PirkejoService = new Services.PirkejoService();
            ConfigurePirkejoDataGrid();
            FillGrid();

        }
        private void FillGrid()
        {
            _uzsakymai = _PirkejoService.GetOrders(_pirkejoID);
            UzsakymuDataGridView.Rows.Clear();
            foreach (var uzsakymai in _uzsakymai)
            {
                int index = UzsakymuDataGridView.Rows.Add();
                UzsakymuDataGridView.Rows[index].Cells["PirkejoID"].Value = uzsakymai.ID;
                UzsakymuDataGridView.Rows[index].Cells["PirkejoVardas"].Value = uzsakymai.Vardas;
                UzsakymuDataGridView.Rows[index].Cells["UzsakymoObjektas"].Value = uzsakymai;
                UzsakymuDataGridView.Rows[index].Cells["PirkejoPavarde"].Value = uzsakymai.Pavarde;
                UzsakymuDataGridView.Rows[index].Cells["UzsakymoData"].Value = uzsakymai.Data;
                UzsakymuDataGridView.Rows[index].Cells["Uzsakymai"].Value = "Uzsakymai";
            }
        }

        public void ConfigurePirkejoDataGrid()
        {
            UzsakymuDataGridView.Columns.Add("PirkejoID", "Pirkejo ID");
            UzsakymuDataGridView.Columns["PirkejoID"].Visible = false;
            UzsakymuDataGridView.Columns["PirkejoID"].Name = "PirkejoID";
            UzsakymuDataGridView.Columns["PirkejoID"].CellTemplate = new DataGridViewTextBoxCell();


            DataGridViewColumn PirkejoVardas = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Vardas",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "PirkejoVardas",
                DataPropertyName = "PirkejoVardas",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            UzsakymuDataGridView.Columns.Add(PirkejoVardas);

            DataGridViewColumn UzsakymoObjektas = new DataGridViewColumn
            {
                Name = "UzsakymoObjektas",
                DataPropertyName = "UzsakymoObjektas",
                CellTemplate = new DataGridViewTextBoxCell(),
                Visible = false
            };
            UzsakymuDataGridView.Columns.Add(UzsakymoObjektas);

            DataGridViewColumn PirkejoPavarde = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Pavarde",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "PirkejoPavarde",
                DataPropertyName = "PirkejoPavarde",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            UzsakymuDataGridView.Columns.Add(PirkejoPavarde);

            DataGridViewColumn PirkejoUzsakymoData = new DataGridViewColumn
            {
                Width = 120,
                HeaderText = "Uzsakymo Data",
                Resizable = DataGridViewTriState.False,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Name = "UzsakymoData",
                DataPropertyName = "UzsakymoData",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            UzsakymuDataGridView.Columns.Add(PirkejoUzsakymoData);

            DataGridViewButtonColumn UzsakymuButton = new DataGridViewButtonColumn
            {
                Name = "Uzsakymai",
                Width = 90,
                HeaderText = " ",
                Text = "Uzsakymai",
                UseColumnTextForButtonValue = true,
                CellTemplate = new DataGridViewButtonCell()
            };
            UzsakymuDataGridView.Columns.Add(UzsakymuButton);

            UzsakymuDataGridView.CellContentClick += UzsakymuDataGridView_CellContentClick;

        }

        private void UzsakymuDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
              
                PirkejoPrekes ppreke = new PirkejoPrekes(_uzsakymai[e.RowIndex]);
                ppreke.Show();
            }
        }
    }
}
